﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SetAndGet
{
    public class Worker
    {
        private int id;

        public int Id
        {
            get
            {
                if(this.id <= 0)
                {
                    throw new Exception("Wrong ID. Only natural numbers");
                }
                else
                {
                   return this.id;
                }
            }
          
            set
            {
                this.id = value;
            }
        }


    }
}
