﻿using System;

namespace SetAndGet
{
    class Program
    {
        static void Main(string[] args)
        {

            Employee obj1 = new Employee();     // basic
            Console.WriteLine(obj1.Name);
            obj1.Name = "Marta";
            Console.WriteLine(obj1.Name);


            Worker obj2 = new Worker();         // only natural number or you will get exception
            obj2.Id = 777;                      // emphasis on get
            Console.WriteLine(obj2.Id);


            Clerk obj3 = new Clerk();           
            Console.WriteLine(obj3.Name);       // nullOrEmpty ? return no name
            obj3.Name = "Zane123456789";        // max lenght 10
            Console.WriteLine(obj3.Name);

            PropWithConstr obj4 = new PropWithConstr("Edijs", 1993);   // properties with constructor.... no fields
            Console.WriteLine(obj4.Name);       // all prop private set
            Console.WriteLine(obj4.Id);
            Console.WriteLine(obj4.ToString());


        }
    }
}
