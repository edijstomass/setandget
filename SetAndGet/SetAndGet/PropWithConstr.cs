﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SetAndGet
{
   public class PropWithConstr
    {
        public string Name { get; private set; }
        public int Id { get; private set; }

        public PropWithConstr(string name, int id)
        {
            this.Id = id;
            this.Name = name;

        }

        public override string ToString()
        {
            return $"Employee name is: {this.Name}, and Employee id is: {this.Id}!";
        }

    }
}
